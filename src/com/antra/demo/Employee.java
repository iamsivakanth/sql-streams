package com.antra.demo;

import java.time.LocalDate;

public class Employee {

	public Employee(Integer id, String name, String gender, Integer age, String dept, LocalDate dateOfJoin,
			Double salary) {

		this.id = id;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.dept = dept;
		this.dateOfJoin = dateOfJoin;
		this.salary = salary;
	}

	private Integer id;
	private String name;
	private String gender;
	private Integer age;
	private String dept;
	private LocalDate dateOfJoin;
	private Double salary;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public LocalDate getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(LocalDate dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", gender=" + gender + ", age=" + age + ", dept=" + dept
				+ ", dateOfJoin=" + dateOfJoin + ", salary=" + salary + "]";
	}
}