package com.antra.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 
 * @author Sivakanth
 *
 */

//Finding Male and Female count in IT and Design Departments

public class FindingMaleAndFemaleEmpInDepts {

	public static void main(String[] args) {

		List<Employee> list = new ArrayList<Employee>();

		list.add(new Employee(1234, "Ravi", "Male", 25, "Management", LocalDate.of(2019, 01, 05), 55000.0));
		list.add(new Employee(1235, "Siva", "Male", 26, "IT", LocalDate.of(2019, 01, 20), 45000.0));
		list.add(new Employee(1236, "Kanth", "Male", 27, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1239, "Priya", "Female", 22, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1237, "Sara", "Female", 22, "Testing", LocalDate.of(2018, 12, 18), 75000.0));
		list.add(new Employee(1238, "Amy", "Female", 20, "Design", LocalDate.of(2018, 01, 8), 65000.0));

		Map<String, Long> map = list.stream()
									.filter(t -> t.getDept().equals("IT") || t.getDept().equals("Design"))
									.collect(Collectors.groupingBy(t -> t.getGender(), Collectors.counting()));

		Map<String, Map<String, Long>> map1 = list.stream()
													.filter(t -> t.getDept().equals("IT") || t.getDept().equals("Design"))
													.collect(Collectors.groupingBy(t -> t.getDept(), 
													Collectors.groupingBy(t -> t.getGender(), Collectors.counting())));
//		System.out.println(map);
		System.out.println(map1);
	}
}