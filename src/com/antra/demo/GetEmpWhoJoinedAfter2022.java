package com.antra.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sivakanth
 *
 */

// Get employee who joined after 2022

public class GetEmpWhoJoinedAfter2022 {

	public static void main(String[] args) {

		List<Employee> list = new ArrayList<Employee>();

		list.add(new Employee(1234, "Ravi", "Male", 25, "Management", LocalDate.of(2019, 01, 05), 55000.0));
		list.add(new Employee(1235, "Siva", "Male", 26, "IT", LocalDate.of(2019, 01, 20), 45000.0));
		list.add(new Employee(1236, "Kanth", "Male", 27, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1237, "Sara", "Female", 22, "Testing", LocalDate.of(2018, 12, 18), 75000.0));
		list.add(new Employee(1238, "Amy", "Female", 20, "Design", LocalDate.of(2018, 01, 8), 65000.0));

		list.stream().filter(t -> t.getDateOfJoin().isAfter(LocalDate.of(2022, 01, 01))).map(t -> t.getName())
				.forEach(System.out::println);
	}
}