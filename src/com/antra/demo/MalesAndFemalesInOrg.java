package com.antra.demo;

/**
 * 
 * @author Sivakanth
 * 
 */

//Finding Number of Male and Female employees in the list

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MalesAndFemalesInOrg {

	public static void main(String[] args) {

		List<Employee> list = new ArrayList<Employee>();

		list.add(new Employee(1234, "Ravi", "Male", 25, "Management", LocalDate.of(2019, 01, 05), 55000.0));
		list.add(new Employee(1235, "Siva", "Male", 26, "IT", LocalDate.of(2019, 01, 20), 45000.0));
		list.add(new Employee(1236, "Kanth", "Male", 27, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1237, "Sara", "Female", 22, "Testing", LocalDate.of(2018, 12, 18), 75000.0));
		list.add(new Employee(1238, "Amy", "Female", 20, "Design", LocalDate.of(2018, 01, 8), 65000.0));

		Map<Object, List<Employee>> map = list.stream().collect(Collectors.groupingBy(t -> t.getGender()));
		System.out.println(map);

		System.out.println(map.get("Female").size());
		System.out.println(map.get("Male").size());
	}
}