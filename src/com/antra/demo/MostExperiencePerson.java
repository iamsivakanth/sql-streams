package com.antra.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sivakanth
 *
 */

//Finding out most experience person

public class MostExperiencePerson {

	public static void main(String[] args) {

		List<Employee> list = new ArrayList<Employee>();

		list.add(new Employee(1234, "Ravi", "Male", 25, "Management", LocalDate.of(2019, 01, 05), 55000.0));
		list.add(new Employee(1235, "Siva", "Male", 26, "IT", LocalDate.of(2019, 01, 20), 45000.0));
		list.add(new Employee(1236, "Kanth", "Male", 27, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1239, "Priya", "Female", 22, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1237, "Sara", "Female", 22, "Testing", LocalDate.of(2018, 12, 18), 75000.0));
		list.add(new Employee(1238, "Amy", "Female", 20, "Design", LocalDate.of(2018, 01, 8), 65000.0));

		System.out.println(list.stream().min((o1, o2) ->o1.getDateOfJoin().compareTo(o2.getDateOfJoin()) ).get());
	}
}