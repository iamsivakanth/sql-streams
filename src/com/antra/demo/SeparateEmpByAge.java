package com.antra.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 
 * @author Sivakanth
 *
 */

//Separating employees based on age 25

public class SeparateEmpByAge {

	public static void main(String[] args) {

		List<Employee> list = new ArrayList<Employee>();

		list.add(new Employee(1234, "Ravi", "Male", 25, "Management", LocalDate.of(2019, 01, 05), 55000.0));
		list.add(new Employee(1235, "Siva", "Male", 26, "IT", LocalDate.of(2019, 01, 20), 45000.0));
		list.add(new Employee(1236, "Kanth", "Male", 27, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1239, "Priya", "Female", 22, "IT", LocalDate.of(2022, 01, 8), 38000.0));
		list.add(new Employee(1237, "Sara", "Female", 22, "Testing", LocalDate.of(2018, 12, 18), 75000.0));
		list.add(new Employee(1238, "Amy", "Female", 20, "Design", LocalDate.of(2018, 01, 8), 65000.0));

		Map<Boolean, List<Employee>> map = list.stream().collect(Collectors.partitioningBy(t -> t.getAge() > 25));
		map.entrySet().stream().filter(t -> t.getKey()).forEach(System.out::println);
		map.entrySet().stream().filter(t -> !t.getKey()).forEach(System.out::println);
		
		System.out.println("=======================================");

		Set<Entry<Boolean, List<Employee>>> entrySet = map.entrySet();

		for (Entry<Boolean, List<Employee>> entry : entrySet) {

			if (entry.getKey())
				System.out.println("Employees above 25");
			else
				System.out.println("Employees below 26");
			List<Employee> empList = entry.getValue();
			for (Employee e : empList) {
				System.out.println(e.getName());
			}
		}
	}
}